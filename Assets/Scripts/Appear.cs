﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Appear : MonoBehaviour {
    //　出現させる敵を入れておく
    [SerializeField] GameObject[] enemys;

    //　次に敵が出現するまでの時間
    [SerializeField] float appearNextTime;

    //　この場所から出現する敵の数
    [SerializeField] int maxNumOfEnemys;

    //　半径
    [SerializeField] int radius;

    //　今何人の敵を出現させたか
    private int numberOfEnemys;

    //　待ち時間計測フィールド
    private float elapsedTime;

    //スタートとwaveの時間
    public float startWait;
    private float waveWait;

    //score表示のための変数
    public Text scoreText;
    private int score; 

    // Use this for initialization
    void Start()
    {
        score = 0;
        UpdateScore();
        numberOfEnemys = 0;
        elapsedTime = 0f;
        //waveの時間を計算
        waveWait = appearNextTime * (maxNumOfEnemys+1);
        StartCoroutine(Waves());
    }
	

    IEnumerator Waves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            Update();
            yield return new WaitForSeconds(waveWait);
            numberOfEnemys = 0;
        }
    }
	
    void Update()
    {
        //　この場所から出現する最大数を超えてたら何もしない
        if (numberOfEnemys >= maxNumOfEnemys)
        {
            return;
        }
        //　経過時間を足す
        elapsedTime += Time.deltaTime;

        //　経過時間が経ったら
        if (elapsedTime > appearNextTime)
        {
            elapsedTime = 0f;

            AppearEnemy();
        }
    }
    //　敵出現メソッド
    void AppearEnemy()
    {
        //　出現させる敵をランダムに選ぶ
        var randomValue = Random.Range(0, enemys.Length);
        //　敵の位置をランダムに決定

        float angle1 = Random.value * 360f;
        float angle2 = Random.value * 360f;
        Vector3 pos = transform.position;
        float x = radius * Mathf.Sin(angle1 * Mathf.Deg2Rad) * Mathf.Cos(angle2 * Mathf.Deg2Rad);

        float y = radius * Mathf.Sin(angle1 * Mathf.Deg2Rad) * Mathf.Sin(angle2 * Mathf.Deg2Rad);

        float z = radius * Mathf.Cos(angle1 * Mathf.Deg2Rad);

        pos.x = x;
        pos.y = y;
        pos.z = z;
        Instantiate(enemys[randomValue], pos, Quaternion.Euler(0f, 0f, 0f));
        numberOfEnemys++;
        elapsedTime = 0f;
    }

    //scoreの描画
    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }
    //scoreの加算
    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }
}
