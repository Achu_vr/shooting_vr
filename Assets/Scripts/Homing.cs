﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Homing : MonoBehaviour {

    public GameObject Player;
    public GameObject homingObj;
    public float Speed;

    // Use this for initialization
    void Start () {
        Player = GameObject.Find ("Player");
        homingObj = GameObject.Find ("Homing");
    }

    // Update is called once per frame
    void Update () {
        this.transform.position = Vector3.MoveTowards (this.transform.position,new Vector3(Player.transform.position.x, Player.transform.position.y,Player.transform.position.z), Speed * Time.deltaTime);
    }
}
 


