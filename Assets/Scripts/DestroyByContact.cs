﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;
    private Appear appear;

    void Start()
    {

            GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            appear = gameControllerObject.GetComponent(typeof(Appear)) as Appear;
        }
        if (appear == null)
        {
            Debug.Log("Cannot find 'appear' script");
        }  
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Bullet")
        {
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(explosion,3f);
        }

        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            // gameController.GameOver();
        }

        appear.AddScore(scoreValue);
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
}