﻿using UnityEngine;
using System.Collections;

public class Shooting : MonoBehaviour
{

    // bullet prefab
    public GameObject bullet;

    // 弾丸発射点
    public Transform muzzle;

    // 弾丸の速度
    public float speed = 1000;

    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent(typeof(AudioSource)) as AudioSource;
    }

    // Update is called once per frame
    void Update()
    {
        // z キーが押された時
        if (Input.GetKeyDown(KeyCode.Z))
        {
            shot();
        }
      
    }
    void shot(){
        // 弾丸の複製
        GameObject bullets = GameObject.Instantiate(bullet) as GameObject;
        audioSource.Play();
        Vector3 force;
        force = this.gameObject.transform.forward * speed;
        // Rigidbodyに力を加えて発射
        bullets.GetComponent<Rigidbody>().AddForce(force);

        // 弾丸の位置を調整
        bullets.transform.position = muzzle.position;
        Destroy(bullets, 3f);
    }

}
