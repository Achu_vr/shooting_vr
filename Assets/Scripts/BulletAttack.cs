﻿using UnityEngine;
using System.Collections;

public class BulletAttack : MonoBehaviour
{

    //　コライダのIsTriggerのチェックを外し物理的な衝突をさせる場合
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
            Destroy(col.gameObject);
        }
    }
}
